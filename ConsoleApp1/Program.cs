﻿using System;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using System.Windows.Input;
using System.Windows.Forms;

namespace ConsoleApp1
{

    class Program
    {
        [STAThreadAttribute]
        static void Main(string[] args)
        {
            Console.WriteLine("Writes the last write date to a json file.");

            string path = "";
            string output = "";
            if (args.Length == 4)
            {
                if (args[0].Equals("-p".ToLower()) == true)
                {
                    path = args[1];
                }
                if (args[2].Equals("-p".ToLower()) == true)
                {
                    path = args[3];
                }
                if (args[0].Equals("-o".ToLower()) == true)
                {
                    output = args[1];
                }
                if (args[2].Equals("-o".ToLower()) == true)
                {
                    output = args[3];
                }
                Console.WriteLine("-p \"{0}\": Path to directory to scan", path);
                Console.WriteLine("-o \"{0}\": output filename, path included", output);
            }
            else
            {
                Console.WriteLine("-p \"Z:/my/path/\": Path to directory to scan");
                Console.WriteLine("-o \"Z:/out/path/filename.json\": output filename, path included");
                return;
            }

            bool invalidate = false;
            while (true)
            {

                Thread.Sleep(1);
                if ((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && Keyboard.IsKeyDown(Key.S))
                {
                    if (invalidate) { continue; };
                    Console.WriteLine("test");
                    invalidate = true;
                    new VersionWriter();
                }
                else
                {
                    invalidate = false;
                }

            }

            //Application.EnableVisualStyles();
            //Application.Run(new Form1());
        }
    }
}
