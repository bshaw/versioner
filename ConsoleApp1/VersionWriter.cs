﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class VersionWriter
    {
        public VersionWriter()
        {
            var t = new DirectoryInfo("E:\\Development\\Projects\\Web\\NetworkEngine\\src");

            var x = new Versions();
            foreach (var i in t.EnumerateFiles())
            {
                x.files.Add(new File(i));
            }
            var file = new FileStream("E:\\Development\\Projects\\Web\\NetworkEngine\\bin\\versions.json", FileMode.Create);
            var writer = new StreamWriter(file);
            writer.Write(JsonConvert.SerializeObject(x));
            writer.Dispose();
            file.Dispose();
        }
    }
}
