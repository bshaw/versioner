﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Versions
    {
        public List<File> files { get; set; }
        public Versions()
        {
            files = new List<File>();
        }
    }
    class File
    {
        public String filename { get; set; }
        public DateTime last { get; set; }
        public File(FileInfo i)
        {
            filename = i.Name;
            last = i.LastWriteTime;
        }
    }
}
